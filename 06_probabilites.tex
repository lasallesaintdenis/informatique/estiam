\documentclass[12pt,french]{beamer}

\input{prez.tex.inc}
%\usepackage{pythontex}
\usepackage{textpos}

%\usetheme{Bergen}
\usetheme{Hannover}

\input{estiam_color_switch.tex.inc}

\author{Vincent-Xavier Jumel}
\institute[ESTIAM]{\pgfuseimage{logotitre}}

\date{hiver 2019}

\title{Mathématiques appliquées à l'informatique}
\subtitle{Notions de probabilités}

\AtBeginSection[]
{
  %\setbeamertemplate{background}{\pgfuseimage{general0}}
  \begin{frame}
    \tableofcontents[currentsection]
  \end{frame}
  %\setbeamertemplate{background}{\pgfuseimage{general1}}
}

\setbeamercolor{sidebar}{bg=}

\definecolor{YellowESTIAM}{rgb}{253,235,0}
\setbeamercolor*{titlelike}{fg=YellowESTIAM}

\begin{document}

\setbeamertemplate{background}{\pgfuseimage{titre}}

\begin{frame}
  \maketitle
\end{frame}

\setbeamercolor*{titlelike}{fg=blue}

\diapotheme{general0}

\begin{frame}
  \begin{block}{Objectifs}
    \begin{itemize}
      \item Connaître le vocubulaire usuel sur les probabilités
        \pause
      \item Calculer la probabilité d'une variable aléatoire suviant une loi
        donnée.
    \end{itemize}
  \end{block}
  \pause
  \begin{block}{Prérequis}
    \begin{itemize}
      \item Notions sur les fonctions
        \pause
      \item Notions d'intégrale
    \end{itemize}
  \end{block}
\end{frame}

\diapotheme{general1}

\section{Vocabulaire}

\subsection{Espace de probabilité et variable aléatoire}

\diapotheme{theorie0}

\begin{frame}
  \begin{definition}[Probabilité]
    \pause
    On appelle probabilité une mesure de poids 1 sur un espace $\Omega$,
    c'est-à-dire une fonction $p$ qui satisfait aux propriétés suivantes :
    \pause
    \begin{itemize}
      \item $p(\Omega) = 1$
        \pause
      \item Si $A$ et $B$ sont disjoints ($A \cap B = ø$), alors $p(A \cup
        B) = p(A) + p(B)$
    \end{itemize}
  \end{definition}
  \pause
  \begin{proposition}
    \pause
    \begin{itemize}
      \item $p(ø) = 0$
        \pause
      \item $p(\overline{A}) = 1 - p(A)$
        \pause
      \item $p(A \cup B) = p(A) + p(B) - p(A \cap B)$
    \end{itemize}
  \end{proposition}
\end{frame}

\diapotheme{theorie1}

\begin{frame}
  \begin{definition}[Variable aléatoire]
    \pause
    Une variable aléatoire est une fonction de $\Omega \to \R^d$, notée
    abusivement $X$ qui à tout événement de $\Omega$ fait correspondre une
    quantité «mesurable».\\
    \pause
    On note $X \colon \Omega \to \R^d$ et en pratique, si $d = 1$, alors on
    dit que $X$ est une variable aléatoire réelle.
  \end{definition}
  \pause
  \begin{exemple}
    \pause
    Dans les jeux de hasards, on considère souvent le gain (ou la perte)
    plutôt que le résultat du tirage.
  \end{exemple}
  \pause
  \begin{block}{Représentation}
    \pause
    \begin{tabular}{c*{4}{|c}}
      $X = k$ & 1 & 2 & 3 & 4 \\ \hline
      $p(X = k)$ & $0,2$ & $0,3$ & $0,4$ & $0,1$
    \end{tabular}
  \end{block}
\end{frame}

\subsection{Fonction de répartition}

\begin{frame}
  \begin{definition}[Fonction de répartion]
    \pause
    La donnée d'un tel tableau dans le cas discret constitue la fonction de
    répartition.\pause \\
    Dans le cas continu, on suppose qu'il existe une fonction $f$, continue,
    telle que $P(X ≤ x) = \int_{t ≤ x} f(t) \diff t$.\\ \pause
    On appelle la fonction $F \colon x \mapsto P(X ≤ x) = \int_{t ≤ x} f(t)
    \diff t$ fonction de répartition.
  \end{definition}
  \begin{remarque}
    Dans ce cas, $p(X = x) = 0$.
  \end{remarque}

\end{frame}

\subsection{Caractéristiques : espérance, variance, écart-type}

\begin{frame}
  \begin{definition}[Espérance]
    \pause
    L'espérance est la moyenne arithmétique des probabilités : \pause \[
    E(X) = \sum_{k \in X(\Omega)} k × p(X = k) . \]
  \end{definition}
  \pause
  \begin{definition}[Variance]
    \pause
    La variance est la moyenne pondérée des écarts à la moyenne : \pause \[
    V(X) = \sum_{k \in X(\Omega)} (k - E(X))^2 × p(X = k) . \] \pause
    L'écart-type $\sigma$ est $\sigma = \sqrt{V(X)}$.
  \end{definition}
\end{frame}
\begin{frame}
  \begin{proposition}
    \pause
    \[ V(X) = E(X - E(X)^2) . \]
  \end{proposition}
  \begin{proposition}[Pour les lois à densités continues]
    \pause
    \begin{itemize}
      \item $E(X) = \int_{X(\Omega)} xf(x) \diff x$
        \pause
      \item  $V(X) = \int_{X(\Omega)} x^2f(x) \diff x$
    \end{itemize}
  \end{proposition}
\end{frame}

\diapotheme{general1}

\section{Conditionnement}

\subsection{Probabilité conditionnelle}

\diapotheme{theorie0}

\begin{frame}
  \begin{definition}[Conditionnement]
    \pause
    On dit que $B$ est conditionné par $A$ lorsque $p(B | A) ≠ p(B)$, avec
    $p(B | A) = \frac{p(A \cap B)}{p(A)}$.
  \end{definition}
\end{frame}

\diapotheme{theorie1}

\subsection{Indépendance}

\begin{frame}
  \begin{theoreme}
    \pause
    Deux événements sont indépendants si et seulement si \[ p(A) × p(B) =
    p(A \cap B) . \]
  \end{theoreme}
  \pause
  \begin{proposition}
    \pause
    Si $A$ et $B$ sont indépendants, alors leurs contraires $\overline{A}$
    et $\overline{B}$ le sont aussi.
  \end{proposition}
\end{frame}

\subsection{Formule des probabilités totales}

\begin{frame}
  \begin{definition}[Partition]
    \pause
    On appelle partition de $\Omega$ des événements deux à deux disjoints
    dont la réunion est égale à $\Omega$.
  \end{definition}
  \begin{proposition}
    \pause
    Si $A_1, A_2, …,  A_n$ est une partition de $\Omega$, alors \pause \[
    p(B) = \sum_{i=1}^n p(B | A_i ) × p(A_i) . \]
  \end{proposition}
\end{frame}

\section{Graphe probabilistes}

\subsection{Arbres pondérés}

\diapotheme{theorie0}

\begin{frame}
\end{frame}

\diapotheme{theorie1}

\subsection{Graphes}

\begin{frame}
\end{frame}

\end{document}
