# Evaluation projet E1
# 18 octobre 2019
# Question 1-4, mise à zero

#!/usr/bin/python
# -*- coding: utf-8 -*-

#variable
colonne = int(4)
ligne = int(3)
#avec l'utilisation des générateurs
tableau = [[j for j in range(ligne)] for i in range(colonne)]
print(tableau)

#mise à zero de tous les éléments
for i in tableau: #pour chaque colonne du tableau
    #print(i)
    for j,v in enumerate(i): #pour toutes les valeurs énumérées dans la colonne
        #j,v représente la position et la valeur pour la fonction énumérate
        #print(j,v)
        i[j] = 0
print(tableau)