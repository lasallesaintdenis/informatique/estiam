from turtle import *
def triangle(x,y):
    home()
    left(90)
    forward(x)
    right(90)
    forward(90)
    color('green','green')
    begin_fill()
    for i in range(3):
        forward(30)
        left(60)
    end_fill()


