# Evaluation projet E1
# 18 octobre 2019
# Question 3-2, grille de démineur

#!/usr/bin/python
# -*- coding: utf-8 -*-

#import des bibliotheques utiles
from tkinter import *
from random import *

#____________________________________
#Définition des fonctions
#____________________________________
#fonction de grille
def grille(case, nb_larg, nb_haut):
    fin_h = nb_larg*case
    fin_v = nb_haut*case
    for i in range(nb_haut):
        canvas.create_line(0, i*case, fin_h, i*case, fill="black")
    for i in range(nb_larg):
        canvas.create_line(i*case, 0, i*case, fin_v, fill="black")

#fonction position du clic de souris
def position_souris(event):
    global nb_cases #pour pouvoir utiliser cette variable dans la fonction
    case_x = int(event.x / case) #numero de colonne
    case_y = int(event.y / case) # numero de ligne
    label2.configure(text="Clic détecté en X =" + str(event.x) + ", Y =" + str(event.y) + " soit la case (" + str(case_x) + "," + str(case_y) +")")
    if(tableau[case_x][case_y] == 1): # Y a t il une mine sur cette case
        #print("il y a une mine")
        canvas.create_oval((case_x*30)+5 , (case_y*30)+5, (case_x*30)+25, (case_y*30)+25, fill="red")
        label.configure(text="Vous êtes tombé sur une mine. VOUS AVEZ PERDU")
        canvas.unbind("<Button-1>") #stoppe l'evenement du click
    else: #il n'y en a pas
        #print("Il n'y a pas de mine")
        nb_mines_vues = 0
        #aller voir dans les cases adjascentes
        for i in range (-1, 2):
            for j in range (-1, 2):
                #print(case_x+i, case_y+j)
                if(case_x+i >= 0 and case_x+i < nb_larg):
                    if(case_y+j >= 0 and case_y+j < nb_haut):
                        #print(case_x + i, case_y + j, tableau[case_x + i][case_y + j])
                        if(tableau[case_x+i][case_y+j] == 1):
                            nb_mines_vues +=1
        canvas.create_rectangle((case_x * 30) + 5, (case_y * 30) + 5, (case_x * 30) + 25, (case_y * 30) + 25, fill="green")
        canvas.create_text((case_x * 30) + 15, (case_y * 30) + 15, text=nb_mines_vues)
        nb_cases -= 1
        print("Il reste "+str(nb_cases)+" a explorer...")
        if(nb_cases == 0):
            label.configure(text="Bravo vous aves trouvé toutes les mines. VOUS AVEZ GAGNE")
            canvas.unbind("<Button-1>") #stoppe l'evenement du click



#case : taille de la case en pixel
#nb_larg : nombre de case dans la largeur
#nb_haut : nombre de case dans la hauteur
nb_larg = 15
nb_haut = 8
case = 30

#détermination des dimension du canvas
larg_canvas = nb_larg*case
haut_canvas = nb_haut*case
#nombre de mines
nb_mines = 18
nb_cases = (nb_haut*nb_larg)- nb_mines
#initialisation de la grille
colonne = int(nb_larg)
ligne = int(nb_haut)
#toutes les cases de la grille contiennent la valeur 0
#Les cases où il y a une mine contient la valeur 1
#avec l'utilisation des générateurs
tableau = [[j for j in range(ligne)] for i in range(colonne)]
#mise à zero de tous les éléments du tableau
for i in tableau: #pour chaque colonne du tableau
    for j,v in enumerate(i): #pour toutes les valeurs énumérées dans la colonne
        i[j] = 0
#tirage au sort de la position aléatoire des mines
nb_mines_placees = nb_mines
while nb_mines_placees>0: #pour le nombre de mines
    #tirage au sort de coordonnées
    ri = randint(0,ligne-1)
    rj = randint(0,colonne-1)
    #print(rj, ri)
    if(tableau[rj][ri] == 0): #vérification s'il n'y a pas déja une mine
        tableau[rj][ri] = 1
        nb_mines_placees -=1
        #print(nb_mines)
#print(tableau)


#Création de la fenêtre principale
fenetre = Tk()
fenetre.title("Démineur")
#Création du Canvas
canvas = Canvas(fenetre, height=haut_canvas, width=larg_canvas, bg="white")
canvas.bind("<Button-1>", position_souris) #evenement du clic de souris qui envoi sur fonction position_souris
canvas.pack(padx=10, pady=10) #affichage avec une marge
#Création d'un Label
texte = "Il y a " +str(nb_mines)+" mines sur ce terrain..."
label = Label(fenetre, text=texte)
label.pack() #affichage label justifié au centre par default
#création d'un second label pour afficher coordonnées souris
label2 = Label(fenetre)
label2.pack()
#demande affichage de la grille
grille(case,nb_larg,nb_haut)
#boucle d'affichage de la fenêtre
fenetre.mainloop()

