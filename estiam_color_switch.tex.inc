\pgfdeclareimage[height=96mm,width=128mm]{titre}{Pictures/1000000000000780000004387F98054018F48CAB}
\pgfdeclareimage[height=96mm,width=128mm]{general0}{Pictures/10000000000007800000043883399D20FA2D68F6}
\pgfdeclareimage[height=96mm,width=128mm]{general1}{Pictures/1000000000000780000004388F66A8268E13DC36}
\pgfdeclareimage[height=96mm,width=128mm]{theorie0}{Pictures/100000000000078000000438BEB5E6A750131399}
\pgfdeclareimage[height=96mm,width=128mm]{theorie1}{Pictures/1000000000000780000004388366F628B1BB7AA8}
\pgfdeclareimage[height=96mm,width=128mm]{pratique0}{Pictures/1000000000000780000004387313027E562D70E5}
\pgfdeclareimage[height=96mm,width=128mm]{pratique1}{Pictures/1000000000000780000004385EF1C71E90699FD6}

\pgfdeclareimage[height=21.6mm,width=38.4mm]{logotitre}{Pictures/100002010000078000000438A929E071D902A549}
\pgfdeclareimage[height=10.8mm,width=19.2mm]{logosmall}{Pictures/100002010000078000000438A929E071D902A549}


\newcommand{\diapotheme}[1]{
  \setbeamertemplate{background}{\pgfuseimage{#1}}
}
