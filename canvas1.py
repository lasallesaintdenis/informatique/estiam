# Evaluation projet E1
# 18 octobre 2019
# Question 2-1, canvas et label

#!/usr/bin/python
# -*- coding: utf-8 -*-

#import de la bibliothèque avec tous ses éléments
from tkinter import *
#Création de la fenêtre
from tkinter import Canvas
#Création de la fenêtre principale
fenetre = Tk()
#Création du Canvas
canvas = Canvas(fenetre, width=200, height=200, bg="white")
canvas.pack() #affichage
#Création d'un Label
label = Label(fenetre, text="Ceci est un label")
label.pack() #affichage
#boucle d'affichage de la fenêtre
fenetre.mainloop()