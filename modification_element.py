# Evaluation projet E1
# 18 octobre 2019
# Question 1-5, modification elements

#!/usr/bin/python
# -*- coding: utf-8 -*-

#variable
colonne = int(4)
ligne = int(3)
#avec l'utilisation des générateurs
tableau = [[j for j in range(ligne)] for i in range(colonne)]
print(tableau)
#mise à zero de tous les éléments
for i in tableau: #pour chaque colonne du tableau
    for j,v in enumerate(i): #pour toutes les valeurs énumérées dans la colonne
        i[j] = 0
print(tableau)

#fonction demandée
def element(i,j): #accés à l'élement ligne i et colonne j
    if(i <= ligne-1 and j<= colonne-1):# vérification si la demande est dans le limites
        tableau[j][i] = -1 # mise à la valeur -1 de l'élément demandé
        #mise à 1 des cases adjacentes
        if(i-1 >= 0): #pour la ligne précedente si elle existe
            tableau[j][i-1] = 1
            if(j-1 >= 0): #pour la colonne précédente si elle existe
                tableau[j-1][i] = 1
                tableau[j-1][i-1] = 1
                if(i+1 <= ligne-1): #ligne suivante
                    tableau[j-1][i+1] = 1
            if (j+1 <= colonne-1):  # pour la colonne suivante si elle existe
                tableau[j+1][i] = 1
                tableau[j+1][i-1] = 1
                if (i+1 <= ligne-1):  # ligne suivante
                    tableau[j+1][i+1] = 1
        if(i+1 <= ligne-1): #pour la ligne suivante si elle existe
            tableau[j][i+1] = 1
            if (j-1 >= 0):  # pour la colonne précédente si elle existe
                tableau[j-1][i] = 1
                tableau[j-1][i+1] = 1
            if (j+1 <= colonne-1):  # pour la colonne suivante si elle existe
                tableau[j+1][i] = 1
                tableau[j+1][i+1] = 1

#appel de la fonction
element(0,0)
print(tableau)