# Evaluation projet E1
# 18 octobre 2019
# Question 3-1, grille de démineur

#!/usr/bin/python
# -*- coding: utf-8 -*-

#fonction de grille
def grille(case, nb_larg, nb_haut):
    fin_h = nb_larg*case
    fin_v = nb_haut*case
    for i in range(nb_haut):
        canvas.create_line(0, i*case, fin_h, i*case, fill = "black")
    for i in range(nb_larg):
        canvas.create_line(i*case, 0, i*case, fin_v, fill="black")


#case : taille de la case en pixel
#nb_larg : nombre de case dans la largeur
#nb_haut : nombre de case dans la hauteur
nb_larg = 15
nb_haut = 8
case = 30
#détermination des dimension du canvas
larg_canvas = nb_larg*case
haut_canvas = nb_haut*case
#nombre de mines
nb_mines = 18

#import de la bibliothèque avec tous ses éléments
from tkinter import *
#Création de la fenêtre principale
fenetre = Tk()
fenetre.title("Démineur")
#Création du Canvas
canvas = Canvas(fenetre, height=haut_canvas, width=larg_canvas, bg="white")
canvas.pack(padx=10, pady=10) #affichage avec une marge
#Création d'un Label
texte = "Il reste " +str(nb_mines)+" mines sur ce terrain..."
label = Label(fenetre, text=texte)
label.pack() #affichage label justifié au centre par default
#demande affichage de la grille
grille(case,nb_larg,nb_haut)
#boucle d'affichage de la fenêtre
fenetre.mainloop()