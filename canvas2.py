# Evaluation projet E1
# 18 octobre 2019
# Question 2-1, canvas et label

#!/usr/bin/python
# -*- coding: utf-8 -*-

#fonction de grille
def grille(case, nb_larg, nb_haut):
    fin_h = nb_larg*case
    fin_v = nb_haut*case
    for i in range(nb_haut):
        canvas.create_line(0, i*case, fin_h, i*case, fill = "black")
    for i in range(nb_larg):
        canvas.create_line(i*case, 0, i*case, fin_v, fill="black")


#case : taille de la case en pixel
#nb_larg : nombre de case dans la largeur
#nb_haut : nombre de case dans la hauteur
nb_larg = 10
nb_haut = 10
case = 40
#détermination des dimension du canvas
larg_canvas = nb_larg*case
haut_canvas = nb_haut*case

#import de la bibliothèque avec tous ses éléments
from tkinter import *
#Création de la fenêtre principale
fenetre = Tk()
fenetre.title("Dessin d'une grille")
#Création du Canvas
canvas = Canvas(fenetre, height=haut_canvas, width=larg_canvas, bg="white")
canvas.pack(padx=10, pady=10) #affichage avec une marge
#Création d'un Label
texte = "Grille de " +str(nb_larg)+" par "+str(nb_haut)
label = Label(fenetre, text=texte)
label.pack(side=LEFT) #affichage label justifié à gauche
#demande affichage de la grille
grille(case,nb_larg,nb_haut)
#boucle d'affichage de la fenêtre
fenetre.mainloop()