# Evaluation projet E1
# 18 octobre 2019
# Question 1-2, Liste de listes

# -*- coding: utf-8 -*-

#variable
colonne = int(4)
ligne = int(3)
#avec l'utilisation des générateurs
tableau = [[j for j in range(ligne)] for i in range(colonne)]
print(tableau)

#utilisation de l'extension numpy
#il faut installer au préalable l'extension par
#python -m pip install numpy
from numpy import zeros
tableau2 = zeros((colonne,ligne),dtype='int') #fonction qui remplie un tableau avec des zéros de type entier
print(tableau2)
for i in range(colonne):
    for j in range(ligne):
        tableau2[i][j] = j
print(tableau2)
