# Evaluation projet E1
# 18 octobre 2019
# Question 1-1, Liste simple

#!/usr/bin/python
# -*- coding: utf-8 -*-

#liste vide pour pouvoir la completer
a = []
#boucle de remplicage
for i in range(0,100):
    a.append(i)
print(a)

# D'une façon plus compact, il faut utiliser les générateurs
b = [i for i in range (100)] # Attention, il faut allé jusqu'a 100 pour avoir de 0 à 99
print(b)
