# Evaluation projet E1
# 18 octobre 2019
# Question 1-3, acces à un element

#!/usr/bin/python
# -*- coding: utf-8 -*-

#variable
colonne = int(4)
ligne = int(3)
#avec l'utilisation des générateurs
tableau = [[j for j in range(ligne)] for i in range(colonne)]
print(tableau)
#accès au dernier élément
print(tableau[-1][-1])


#pour aller plus loin
tableau2 = [[0,1,2],[3,4,5],[6,7,8],[9,10,11]]
print(tableau2)
#on peut accéder à la dernière colonne
print(tableau2[-1])
#ou la deuxieme
print(tableau2[1])
#acces à l'élément 1 de la colonne3
print(tableau2[2][0])

